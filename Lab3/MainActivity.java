package com.r.currencyconversion;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity
{
    // Declare Variables
    Button converterUSDToYen;
    Button converterYenToUSD;
    TextView inputCurrency;
    TextView outputCurrency;
    final double usCurrency = 0.009309;
    double usCurrencyInput;
    double japaneseCurrency ;
    double japaneseCurrencyInput;
    double output;  // original name 'conversion'

    String url = "https://api.fixer.io/latest?base=USD";
    String json = "";
    String line = "";
    String rate = "";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Declare and Initialize BackgroundTask Object
        BackgroundTask object = new BackgroundTask();

        // Invoke execute method from the AsynchTask extension in the Background Class
        object.execute();
        // Initialize Buttons and Texts
        converterUSDToYen = findViewById(R.id.ConvertUSD);
        converterYenToUSD = findViewById(R.id.ConvertYen);
        inputCurrency = findViewById(R.id.InputCurrency);
        outputCurrency = findViewById(R.id.OutputCurrency);

        inputCurrency.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // Clear Input Text on touch
                inputCurrency.setText("");
            }
        });

        converterUSDToYen.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                System.out.println("\nTESTING 1 ... Before AnyschExecution\n");



                System.out.println("\nTesting 2 ... After AsynchExecution\n");

                // Change input string to float
                usCurrencyInput = Double.parseDouble(inputCurrency.getText().toString());

                // Calculate currency conversion
                output = usCurrencyInput * japaneseCurrency;

                // Output Currency Conversion
                outputCurrency.setText("$" + Double.toString(Math.round(usCurrencyInput * 100.0) / 100.0) + " = " + Double.toString(Math.round(output * 100.0) / 100.0) + " Yen");
            }
        });

        converterYenToUSD.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // Change input string to float
                japaneseCurrencyInput = Double.parseDouble(inputCurrency.getText().toString());

                // Calculate currency conversion
                output = usCurrency * japaneseCurrencyInput;

                // Output Currency Conversion
                outputCurrency.setText(Double.toString(Math.round(japaneseCurrencyInput * 100.0) / 100.0) + " Yen = $" + Double.toString(Math.round(output * 100.0) / 100.0));
            }
        });
    }

    private class BackgroundTask extends AsyncTask<Void, Void, String>
    {
        @Override
        protected void onPreExecute() {super.onPreExecute();}
        @Override
        protected String doInBackground(Void... params)
        {
            try
            {
                // Create an object from the URL Class and initialize it to the string 'url' in this Java Class, MainActivity
                URL web_url = new URL(MainActivity.this.url);

                // Create an Object from the HttpURLConnection class named httpURLConnection and initialize it with (HttpURLConnection)web_url.openConnection()
                // where the method openConnection() is a method defined in the URL class
                HttpURLConnection httpURLConnection = (HttpURLConnection)web_url.openConnection();

                // Request method set as GET
                httpURLConnection.setRequestMethod("GET");

                System.out.println("\nTESTING ... BEFORE connection method to URL\n");

                // Invoke the connect() method from the object httpURLConnection
                httpURLConnection.connect();

                // Create an object from the class InputStream and initialize object with
                InputStream inputStream = httpURLConnection.getInputStream();

                // Create Object named bufferedReader from the BufferedReader class and initialize the object with new BufferedReader(new InputStreamReader(inputStream))
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                System.out.println("Connection Successful\n");

                // Extract the string from the JSON, line by line, store it in the 'json' string variable, using a while loop, checking until the end of the entire JSON String
                while (line != null)
                {
                    // We will assign the bufferedReader.readLine() to the string line every iteration
                    line = bufferedReader.readLine();

                    // then we will append line to json
                    json += line;
                }
                System.out.println("\nTHE JSON: " + json);

                // Create JSON Object from JSONObject Class, using the json string
                JSONObject obj = new JSONObject(json);

                // Create second JSON Object that will contain a nested JSON Object within the first JSON Object created
                JSONObject objectRate = obj.getJSONObject("rates");

                // Use the second JSON Object created and use the get(Sring ...) method
                // Put "JPY" as the argument in the parameter of the get(String ...) method in order to get the exchange rate for Yen
                // The logic code for the currency conversion must go into the onPostExecute() Method within AsyncTask
                rate = objectRate.get("JPY").toString();
                Log.i("test", "This is the rate : " + rate);
                // Obtain the latest currency-conversion value from URL, convert from String to Double
                double convert = Double.parseDouble(rate);
            }
            catch (MalformedURLException e)
            {
                e.printStackTrace();;
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            catch (JSONException e)
            {
                Log.e("MYAPP", "unexpected JSON exception", e);
                System.exit(1);
            }
            return rate;
        }

        protected void onPostExecute(String result)
        {
            super.onPostExecute(result);
            // the string rate will store the Yen to USD conversion ratio

            System.out.println("\nWhat is rate: " + result + "\n");

            // Convert the String 'rate' to the type double 'value' HERE, within the Asynchronous Task
            double value = Double.parseDouble(result);
            //setting the yen
            japaneseCurrency = value;
            System.out.println("\nTesting JSON String Exchange Rate INSIDE AsynchTask: " + value);

            // Change input string to float
            //usCurrencyInput = Double.parseDouble(inputCurrency.getText().toString());

            // Calculate currency conversion
            //output = usCurrencyInput * japaneseCurrency;

            // Output Currency Conversion
            //    outputCurrency.setText("$" + Double.toString(Math.round(usCurrencyInput * 100.0) / 100.0) + " = " + Double.toString(Math.round(output * 100.0) / 100.0) + " Yen");
        }

    }
}
