package com.r.currencyconversion;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
{
    // Declare Variables
    Button converter;
    TextView inputUSD;
    TextView outputYen;
    float usCurrency;
    float japaneseCurrency = 106.93F;
    float conversion;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initialize Buttons and Texts
        converter = findViewById(R.id.Convert);
        inputUSD = findViewById(R.id.USDInput);
        outputYen = findViewById(R.id.OutputYen);

        inputUSD.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // Clear Input Text on touch
                inputUSD.setText("");
            }
        });

        converter.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // Change input string to float
                usCurrency = Float.parseFloat(inputUSD.getText().toString());

                // Calculate currency conversion
                conversion = usCurrency * japaneseCurrency;

                // Output Currency Conversion
                outputYen.setText("$" + Float.toString(usCurrency) + " = " + Float.toString(conversion) + " Yen");
            }
        });
    }
}
