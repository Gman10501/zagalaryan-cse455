package com.r.currencyconversion;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
{
    // Declare Variables
    Button converterUSDToYen;
    Button converterYenToUSD;
    TextView inputCurrency;
    TextView outputCurrency;
    final double usCurrency = 0.009309;
    double usCurrencyInput;
    final double japaneseCurrency = 112.57;
    double japaneseCurrencyInput;
    double conversion;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initialize Buttons and Texts
        converterUSDToYen = findViewById(R.id.ConvertUSD);
        converterYenToUSD = findViewById(R.id.ConvertYen);
        inputCurrency = findViewById(R.id.InputCurrency);
        outputCurrency = findViewById(R.id.OutputCurrency);

        inputCurrency.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // Clear Input Text on touch
                inputCurrency.setText("");
            }
        });

        converterUSDToYen.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // Change input string to float
                usCurrencyInput = Double.parseDouble(inputCurrency.getText().toString());

                // Calculate currency conversion
                conversion = usCurrencyInput * japaneseCurrency;

                // Output Currency Conversion
                outputCurrency.setText("$" + Double.toString(Math.round(usCurrencyInput * 100.0) / 100.0) + " = " + Double.toString(Math.round(conversion * 100.0) / 100.0) + " Yen");
            }
        });

        converterYenToUSD.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // Change input string to float
                japaneseCurrencyInput = Double.parseDouble(inputCurrency.getText().toString());

                // Calculate currency conversion
                conversion = usCurrency * japaneseCurrencyInput;

                // Output Currency Conversion
                outputCurrency.setText(Double.toString(Math.round(japaneseCurrencyInput * 100.0) / 100.0) + " Yen = $" + Double.toString(Math.round(conversion * 100.0) / 100.0));
            }
        });
    }
}
